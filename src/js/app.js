import UrlParser from './routings/url-parser.js'
import routes from './routings/routes.js'

const renderPage = async () => {
  const url = UrlParser.parseActiveUrlWithCombiner()
  const page = routes[url]

  document.querySelector('#page').innerHTML = await page.render()
  await page.afterRender()
}

window.addEventListener('hashchange', renderPage)
window.addEventListener('load', renderPage)
