import Main from '../pages/main.js'
import Cart from '../pages/cart.js'
import Success from '../pages/success.js'
import History from '../pages/history.js'
import HistoryDetail from '../pages/historyDetail.js'
import PromoCode from '../pages/promoCode.js'

const routes = {
    '/': Main,
    '/cart': Cart,
    '/success/:id': Success,
    '/history': History,
    '/history/:id': HistoryDetail,
    '/promo': PromoCode
}

export default routes