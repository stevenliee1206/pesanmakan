const Main = {
    async render() {
        return `
        <div class="my-2 d-flex flex-row justify-content-between justify-content-lg-center align-items-center">
          <div>
            <p class="mx-4 mb-0" style="font-size: 16px; color: #B6B6B6; font-family: 'Passion One', cursive;">Welcome Back,</p>
            <h3 id="lineName" class="mx-4 mb-0" style="font-size: 35px; color: black; font-family: 'Passion One', cursive; margin-top: -5px;">User</h3>
          </div>
          <img id="profile-img" class="card-img mx-4" style="border-radius: 50%; width: 60px; height: 60px;" src='https://www.jobstreet.co.id/en/cms/employer/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png'>
        </div>

        <div class="tabs">
        <input type="radio" id="tab1" name="tab-control" checked>
        <input type="radio" id="tab2" name="tab-control">
        <input type="radio" id="tab3" name="tab-control">
        <input type="radio" id="tab4" name="tab-control">
        <ul class="px-4 pt-3">
            <li title="Burger" class="p-0">
                <label for="tab1" role="button">
                    <i class="fas fa-hamburger"></i>
                    <br><span> Burger</span>
                </label>
            </li>
            <li title="Pizza" class="p-0">
                <label for="tab2" role="button">
                    <i class="fas fa-pizza-slice"></i><br><span> Pizza</span>
                </label>
            </li>
            <li title="Bread" class="p-0">
                <label for="tab3" role="button">
                    <i class="fas fa-bread-slice"></i><br><span> Bread</span>
                </label>
            </li>
            <li title="Cake" class="p-0">
                <label for="tab4" role="button">
                    <i class="fas fa-birthday-cake"></i><br><span> Cake</span>
                </label>
            </li>
        </ul>
        <div class="slider mx-4">
            <div class="indicator"></div>
        </div>
        <div class="content" id="menus" style="margin-bottom: 50px !important"></div>
        <div id="my-nav"></div>
        `
    },

    async afterRender() {
        const myMenus = []
        fetch("../../../src/sources/data.json")
            .then((res) => res.json())
            .then((res) => {
                res.forEach(({
                    type,
                    menus
                }) => {
                    const base = document.querySelector("#menus")
                    base.innerHTML += `
                        <section class="section-background">
                        <h2 class="px-4 pt-3">${type}</h2>
                        <div id="${type}" class="carousel-container d-flex flex-nowrap position-relative" style="overflow-x: scroll;"></div>
                    `
                    if (menus.length) {
                        const parent = document.querySelector(`#${type}`)
                        menus.forEach(({
                            id,
                            name,
                            time,
                            price,
                            rating
                        }) => {
                            parent.innerHTML += `
                                <div class="card menu shadow" style="background: linear-gradient(180deg, rgba(255,180,78,1) 0%, rgba(255,205,90,1) 47%, rgba(255,226,100,1) 100%);">
                                <i class="fas fa-star position-absolute" style="color: rgba(255,255,255,0.3); top: -15px; left: -15px; font-size: 100px; transform: rotate(15deg);"></i>
                                <p class="m-0 position-absolute" style="top: 15px; left: 20px; font-size: 30px; color:#7F0000; font-weight: bold;">${rating}</p>
                                <div class="w-100 d-flex justify-content-center mt-5 position-relative">
                                    <div style="width: 200px; height: 200px; position: relative;">
                                        <img class="card-img" style="border-radius: 50%;" src="../../src/assets/images/${id}.jpg" alt="${name}">
                                        <div class="rounded-border d-flex align-items-center justify-content-center" id="prize">
                                            <strong style="color: white">${price / 1000}k</strong>
                                        </div>
                                    </div>
                                </div>
                                <p class="px-5 mt-4" style="color: #7F0000;" id="food-name">
                                    <strong>${name}</strong>
                                    <strong style="font-size: 15px;color: #796d67"> - ${time}</strong>
                                </p>
                                <div class="input-group" id="${id}">
                                    <button class="min-button mr-5 min-item d-none qtychange" data-id="${id}">
                                        <i class="fas fa-minus" style="color: #F3CC3C;"></i>
                                    </button>
                                    <input name="qty[${id}]" type="text" class="form-control d-none" readonly>
                                    <button class="add-button ml-5 add-item qtychange" data-id="${id}">
                                        <i class="fas fa-plus" style="color: #F3CC3C;"></i>
                                    </button>
                                </div>
                            </div>
                            `
                        })
                    }
                    myMenus.push(...menus)
                })
            })
            .then(() => {
                const cartAction = document.querySelector("#my-nav")
                cartAction.innerHTML = `
                    <div id="isLogin" class="px-4 lg-px-5 w-100 mx-auto d-flex flex-row justify-content-around" style="position: fixed; bottom: 0; left: 0; background-color: white !important;">
                        <div class="d-flex align-items-center flex-column">
                            <a class="btn btn-lg m-2 mb-0" href="/#/cart" style="color: #929DAF; font-size: 16px;"><i class="fas fa-shopping-cart"></i></a>
                            <p class="mb-1" style="font-size: 15px; margin-top: -8px; font-family: 'Passion One', cursive; color: #929DAF">Cart</p>
                        </div>
                        <div class="d-flex align-items-center flex-column">
                            <a href="/#/history" id="history" class="btn btn-lg m-2 mb-0" style="color: #929DAF; font-size: 16px;"><i class="fas fa-history"></i></a>
                            <p class="mb-1" style="font-size: 15px; margin-top: -8px; font-family: 'Passion One', cursive; color: #929DAF">History</p>
                        </div>
                        <div class="d-flex align-items-center flex-column">
                            <a id="empty-cart" class="btn btn-lg m-2 mb-0" style="color: #929DAF; font-size: 16px;"><i class="fas fa-trash"></i></a>
                            <p class="mb-1" style="font-size: 15px; margin-top: -8px; font-family: 'Passion One', cursive; color: #929DAF">Delete Cart</p>
                        </div>

                    </div>
                `
            })

            .then(() => {
                document.querySelectorAll('button.qtychange').forEach((item) => {
                    item.addEventListener('click', () => {
                        const id = item.getAttribute('data-id')

                        if (item.classList.contains('add-button')) {
                            changeQty(id, 1)
                        } else if (item.classList.contains('min-button')) {
                            changeQty(id, -1)
                        }
                    })
                })

                document.querySelector('#empty-cart').addEventListener('click', () => {
                    if (sessionStorage.getItem('clientOrder').length !== 2) {
                        Swal.fire({
                            title: 'Are you sure?',
                            text: "You won't be able to revert this!",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, delete it!'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                Swal.fire(
                                    'Deleted!',
                                    'Your file has been deleted.',
                                    'success'
                                )
                                sessionStorage.setItem('clientOrder', JSON.stringify([]))
                                sessionStorage.setItem('promoOrder', JSON.stringify(""))
                                setTimeout(function () {
                                    location.reload()
                                }, 900);
                            }
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Your Cart is Empty!'
                        })
                    }
                })
            })
            .then(() => {
                loadData()
            })

        const changeQty = (id, point) => {
            const menuItem = document.querySelector(`input[name="qty[${id}]"]`)
            const itemQty = menuItem.value ? parseInt(menuItem.value) : 0
            const menuItemAction = menuItem.parentElement

            if (point > 0) {
                if (itemQty === 0) {
                    menuItemAction.querySelectorAll('.d-none').forEach((action) => {
                        action.classList.remove('d-none')
                        action.classList.add('d-block')
                        document.querySelector(`#${id}`).classList.add("px-2")
                    })
                }
            } else if (point < 0) {
                if (itemQty === 0) {
                    return
                }

                if (itemQty === 1) {
                    menuItemAction.querySelectorAll('.d-block').forEach((action) => {
                        action.classList.remove('d-block')
                        action.classList.add('d-none')
                        document.querySelector(`#${id}`).classList.remove("px-2")
                    })
                }
            }

            const menuItemDetail = getPrice(id)

            menuItem.value = itemQty + point
            menuItemDetail.qty = itemQty + point

            let found = false
            let clientOrder = JSON.parse(sessionStorage.getItem('clientOrder')) || []
            clientOrder.forEach((menu) => {
                if (menu.id == menuItemDetail.id) {
                    menu.qty = menuItemDetail.qty
                    found = true
                    return
                }
            })

            clientOrder = clientOrder.filter((menu) => menu.qty > 0)

            if (!found) {
                sessionStorage.setItem('clientOrder', JSON.stringify([
                    ...clientOrder,
                    menuItemDetail,
                ]))
            } else {
                sessionStorage.setItem('clientOrder', JSON.stringify(clientOrder))
            }
        }

        const getPrice = (searchId) => {
            const {
                id,
                name,
                price
            } = myMenus.filter(({
                id
            }) => id === searchId)[0]
            return {
                id,
                name,
                price
            }
        }

        const loadData = () => {
            const clientOrder = JSON.parse(sessionStorage.getItem('clientOrder')) || []

            if (clientOrder.length) {
                clientOrder.map(({
                    id,
                    qty
                }) => changeQty(id, qty))
            }
        }
    },
}
export default Main