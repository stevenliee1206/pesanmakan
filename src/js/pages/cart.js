const Cart = {
    async render() {
        return `
        <div id="greetingForLogined" class="my-2 d-flex flex-row justify-content-between d-none justify-content-lg-center align-items-center">
            <div>
                <p class="mx-4 mb-0" style="font-size: 16px; color: #B6B6B6; font-family: 'Passion One', cursive;">Welcome Back,</p>
                <h3 id="lineName" class="mx-4 mb-0" style="font-size: 35px; color: black; font-family: 'Passion One', cursive; margin-top: -5px;"></h3>
            </div>
            <img id="profile-img" class="card-img mx-4" style="border-radius: 50%; width: 60px; height: 60px;" src='https://www.jobstreet.co.id/en/cms/employer/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png'>
        </div>
        <div class="row my-3 mx-lg-auto container">
          <div class="d-flex flex-row align-items-center">
            <a id="backtomain" style="border: none; background-color: transparent; color: black">
                <i class="fas fa-chevron-left"></i>
            </a>
            <h2 class="mb-0 mx-3" style="color: black; font-family: 'Passion One', cursive;">My Cart</h2>
            <hr>
          </div>
        </div>
        <div id="orderCart" class="mt-4 container-lg" style="margin-bottom: 125px"></div>
      `
    },

    async afterRender() {
        const clientOrder = JSON.parse(sessionStorage.getItem('clientOrder')) || []
        const promoCode = JSON.parse(sessionStorage.getItem('promoOrder')) || ""

        let total = 0
        let item = 0
        let beforeDisc = 0

        if (clientOrder.length) {
            let i = 0
            clientOrder.forEach(({
                id,
                name,
                price,
                qty
            }) => {
                beforeDisc += (price * qty)
                if (promoCode.toLowerCase() == "makanaja 20%") {
                    total += (price * qty * 0.8)
                } else {
                    total = beforeDisc
                }

                console.log(promoCode.toLowerCase())
                document.querySelector("#orderCart").innerHTML += `
                    <div class="d-flex flex-row" style="margin-bottom: ${i === clientOrder.length - 1 ? "200px" : "0"}">
                        <div class=${i === 0 ? '"px-3 mt-2"' : '"px-3 mt-4"'} style="padding-right: 10px !important">
                            <img src="../../src/assets/images/${id}.jpg" width="125" height="125" style="border-radius: 8px; filter: drop-shadow(1px 4px 6px #E2E3E7);">
                        </div>
                        <div class="d-flex flex-row mt-4 align-items-center">
                            <div class="flex-column">
                                <p class="mx-3 mb-0" style="color: #303434; font-family: 'Viga', sans-serif;">${name}</p>
                                <p class="mx-3" style="color: #A8ABB0; font-family: 'Viga', sans-serif; font-size: 13px;">You order: ${qty}</p>
                                <p class="mx-3 mb-0 mt-4" style="color: #F78430; font-family: 'Viga', sans-serif;"><strong>Rp.${price * qty}</strong></p>
                            </div>
                        </div>
                        </div>
                    </div>
                `
                i++
                
                item += qty
                if (i == clientOrder.length) {
                    document.querySelector("#orderCart").innerHTML += `
                        <div class="w-100 p-3 pt-1 mx-auto" style="position: fixed; bottom: 0; right: 0; border-radius: 30px 30px 0 0; background-color: white">
                            <div class="d-flex justify-content-between flex-row mt-3 justify-content-lg-center align-items-baseline">
                                <p style="font-family: 'Viga', sans-serif; font-size: 0.8rem;">Promo code: &nbsp</p>
                                <button id="promo-code" class="p-2" style="font-family: 'Viga'; background-color: #E2E2E2; border: none; border-radius: 8px; font-size: 14px">
                                    <a href="/#/promo" id="promoCode" style="font-size: 12px; color: #000; text-decoration: none"></a>
                                </button>
                            </div>

                            <div class="d-flex justify-content-between flex-row justify-content-lg-center align-items-baseline">
                                <p style="font-family: 'Viga', sans-serif; font-size: 0.8rem;">Total Payment: &nbsp</p>
                                <div>
                                    <p style="color: #F78430; font-family: 'Viga', sans-serif;">
                                        <s id="beforeDisc" class=${promoCode == "" ? "d-none" : "d-inline-block"} style="font-size: 12px; color: grey;">Rp.${beforeDisc}</s> 
                                        <strong id="afterDisc">Rp.${promoCode.toLowerCase() == "hemat15 15rb" ? total - 15000 : total}</strong>
                                    </p>
                                </div>
                            </div>
                            <div class="d-grid gap-2 col-12 col-md-7 mx-auto my-1">
                                <button style="border-radius: 25px; border: none; background-color: #F78430 !important; color: white;" class="btn btn-warning" id="checkout" type="button">
                                    Checkout
                                </button>
                            </div>
                        </div>
                    `
                }
            })

            document.getElementById("promoCode").text = promoCode === "" ? "Select Promo Code" : promoCode

            document.querySelector('#checkout').addEventListener('click', () => {
                const promo = JSON.parse(sessionStorage.getItem('promoOrder')) || ""
                const historyOrder = JSON.parse(localStorage.getItem('historyOrder')) || []
                const clientOrder = JSON.parse(sessionStorage.getItem('clientOrder'))
                const timestamp = Date.now()

                historyOrder.push({
                    promo,
                    timestamp,
                    menus: clientOrder,
                    total,
                })

                localStorage.setItem('historyOrder', JSON.stringify(historyOrder))
                sessionStorage.setItem('clientOrder', JSON.stringify([]))
                sessionStorage.setItem('promoOrder', JSON.stringify(""))

                location.replace(`/#/success/${timestamp}`)
            })
        } else {
            document.querySelector("#orderCart").innerHTML += `
                <div class="d-flex flex-lg-row flex-column align-items-center" style="position: fixed; left: 50%; transform: translate(-50%, -50%); top: 50%;">
                    <img style="width: 300px; height: 300px" src="../../src/assets/images/empty-cart.png" class=""/>
                    <div class="mx-5">
                        <p style="font-family: 'Viga', sans-serif; color: #3A3128;" >Hey, Your cart is empty :(. Order Now!!</p>
                        <a style="border-radius: 25px; border: none; background-color: #F78430 !important; color: white;" class="btn btn-success w-50" id="orderEmptytohome">Order</a>
                    </div>
                </div>
            `

            document.querySelector("#orderEmptytohome").addEventListener('click', (event) => {
                event.preventDefault()
                location.replace("/#")
                location.reload()
            })
        }

                    
        document.querySelector("#backtomain").addEventListener('click', (event) => {
            event.preventDefault()
            location.replace("/#")
            location.reload()
        })
    }
}

export default Cart