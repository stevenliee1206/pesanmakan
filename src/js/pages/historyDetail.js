import UrlParser from '../routings/url-parser.js'

const HistoryDetail = {
    async render() {
        return `
        <div id="greetingForLogined" class="my-2 d-flex flex-row justify-content-between d-none justify-content-lg-center align-items-center">
            <div>
            <p class="mx-4 mb-0" style="font-size: 16px; color: #B6B6B6; font-family: 'Passion One', cursive;">Welcome Back,</p>
            <h3 id="lineName" class="mx-4 mb-0" style="font-size: 35px; color: black; font-family: 'Passion One', cursive; margin-top: -5px;"></h3>
            </div>
            <img id="profile-img" class="card-img mx-4" style="border-radius: 50%; width: 60px; height: 60px;" src="" alt="">
        </div>
        
        <div class="row my-3 mx-lg-auto container">
          <div class="d-flex flex-row align-items-center">
            <a href="javascript:history.go(-1)" style="border: none; background-color: transparent; color: black">
                <i class="fas fa-chevron-left"></i>
            </a>
            <h2 class="mb-0 mx-3" style="color: black; font-family: 'Passion One', cursive;">Detail Transaction</h2>
            <hr>
          </div>
        </div>
        <div id="transactionDetail" class="mt-4 container-lg" style="margin-bottom: 70px"></div>
      `
    },

    async afterRender() {
        const url = UrlParser.parseActiveUrlWithoutCombiner()
        let historyOrder = JSON.parse(localStorage.getItem('historyOrder')) || []

        historyOrder = historyOrder.filter(({ timestamp }) => timestamp == url.id)[0]
        const historyTotal = historyOrder.total
        let promoCode = historyOrder.promo
        const timestamp = historyOrder.timestamp
        let i = 0
        let total = 0
        let beforeDisc = 0
        historyOrder.menus.forEach(({id, name, price, qty}) => {
            if (i === 0) {
                document.querySelector("#transactionDetail").innerHTML += `
                <div class="row">
                    <div class="col col-9">
                        <div class="d-flex flex-column">
                            <p class="mx-3 mb-0" style="color: #303434; font-family: 'Viga', sans-serif;">Order Id: ${timestamp}</p>
                            <p class="mx-3" style="color: #A8ABB0; font-family: 'Viga', sans-serif; font-size: 15px">${moment(timestamp).format('LL')}</p>
                        </div>
                    </div>
                    <div class="col col-3 text-center m-auto">
                        <p style="color: #5BAC84; font-family: 'Viga', sans-serif;"><i class="far fa-check-circle"></i> Success</p>
                    </div>
                </div>

                <p class="mx-3 mb-0" style="color: #303434; font-family: 'Viga', sans-serif;">Order Items: </p>
                `
            }
            beforeDisc += (price * qty)

            if (promoCode.toLowerCase() == "makanaja 20%") {
                total += (price * qty * 0.8)
            } else {
                total = beforeDisc
            }
            
            document.querySelector("#transactionDetail").innerHTML += `
                <div class="d-flex flex-row" style="margin-bottom: ${i === historyOrder.menus.length - 1 ? "120px" : "0"}">
                    <div class=${i == 0 ? '"px-3 my-2"' : '"px-3 my-3"'} style="padding-right: 10px !important">
                        <img src="../../src/assets/images/${id}.jpg" width="125" height="125" style="border-radius: 8px; filter: drop-shadow(1px 4px 6px #E2E3E7);">
                    </div>
                    <div class="d-flex flex-row mt-4 align-items-center">
                        <div class="flex-column">
                            <p class="mx-3 mb-0" style="color: #303434; font-family: 'Viga', sans-serif;">${name}</p>
                            <p class="mx-3" style="color: #A8ABB0; font-family: 'Viga', sans-serif; font-size: 13px;">You order: ${qty}</p>
                            <p class="mx-3 mt-4" style="color: #F78430; font-family: 'Viga', sans-serif;"><strong>Rp.${price * qty}</strong></p>
                        </div>
                    </div>
                    </div>
                </div>
            `
            i++
            if (i === historyOrder.menus.length) {
                document.querySelector("#transactionDetail").innerHTML += `
                    <div class="w-100 px-3 pt-2 py-1 mx-auto" style="position: fixed; bottom: 0; right: 0; border-radius: 30px 30px 0 0; background-color: white">
                        <div class="d-flex justify-content-between flex-row mt-3 justify-content-lg-center align-items-baseline ${promoCode == "" ? "d-none" : "d-inline-block"}">
                            <p style="font-family: 'Viga', sans-serif; font-size: 0.8rem;">Promo code: &nbsp</p>
                            <button id="promo-code" class="p-2" style="font-family: 'Viga'; background-color: #E2E2E2; border: none; border-radius: 8px; font-size: 12px">
                                ${promoCode}
                            </button>
                        </div>

                        <div class="d-flex justify-content-between flex-row justify-content-lg-center align-items-baseline ${promoCode == "" ? "pt-2" : "pt-0"}">
                            <p style="font-family: 'Viga', sans-serif; font-size: 0.8rem;">Total Payment: &nbsp</p>
                            <p style="color: #F78430; font-family: 'Viga', sans-serif;">
                            <s id="beforeDisc" class=${promoCode == "" ? "d-none" : "d-inline-block"} style="font-size: 12px; color: grey;">Rp.${beforeDisc}</s> 
                            <strong id="afterDisc">Rp.${promoCode.toLowerCase() == "hemat15 15rb" ? total - 15000 : total}</strong>
                        </p>
                        </div>
                    </div>
                `
            }
        })

    }
}

export default HistoryDetail