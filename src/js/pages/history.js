const History = {
    async render() {
        return `
        <div class="row my-3 mx-lg-auto container">
          <div class="d-flex flex-row align-items-center">
            <a id="historytomain" style="border: none; background-color: transparent; color: black">
                <i class="fas fa-chevron-left"></i>
            </a>
            <h2 class="mb-0 mx-3" style="color: black; font-family: 'Passion One', cursive;">Transaction Histories</h2>
            <hr>
          </div>
        </div>
        <div id="history" class="mt-4 container-lg"></div>
      `
    },
    async afterRender() {
        const historyOrder = (JSON.parse(localStorage.getItem('historyOrder')) || []).reverse()

        if (historyOrder.length) {
            historyOrder.forEach(({
                timestamp,
                total
            }) => {
                const myDate = Math.abs(new Date().getDate(timestamp) - new Date().getDate())
                const date = moment(timestamp).subtract(myDate, 'days').calendar()
                let i = 0
                document.querySelector("#history").innerHTML += `
                    <div class="position-relative mb-3">
                        <div class="d-flex flex-row align-items-center py-3 show-detail" style="background-color: white; border-radius: 10px;" id="item-${timestamp}">
                            <div class=${i == 0 ? '"px-3 my-auto"' : '"px-3 my-auto"'} style="padding-right: 10px !important">
                                <img src="../../src/assets/images/checked.png" width="75" height="75" style="border-radius: 8px; filter: drop-shadow(1px 4px 6px #E2E3E7);">
                            </div>
                            <div class="d-flex flex-row  align-items-center">
                                <div class="flex-column">
                                <p class="mx-3 mb-0" style="color: #303434; font-family: 'Viga', sans-serif;">Order Id: ${timestamp}</p>
                                    <p class="mx-3 mb-0" style="color: #F78430; font-family: 'Viga', sans-serif;"><strong>Total: Rp.${total}</strong></p>
                                    <p class="mx-3 mb-0" style="color: #A8ABB0; font-family: 'Viga', sans-serif; font-size: 13px;"><em>${date}</em></p>
                                </div>
                            </div>
                        </div>
                        <div id="z-index-removal-${timestamp}" class="my-z-index d-flex justify-content-end position-absolute row mx-0" style="background-color: transparent; top: 1px; bottom: 1px; right: 1px; left: 1px;border-radius: 10px;">
                            <div class="col col-8 col-md-9 col-lg-10 col-xl-11 px-0 h-100" style="opacity: 0"></div>
                            <div class="col col-4 col-md-3 col-lg-2 col-xl-1 px-0 d-flex flex-column h-100" style="opacity: 1">
                                <button id="show-${timestamp}"class="d-flex flex-column align-items-center my-auto gotodetail show-it w-100 h-100 justify-content-center" style="background-color: white; border: none; border-radius: 0 10px 0 0">
                                    <a href="/#/history/${timestamp}" style="color: #6CBD0C;font-size: 20px;"><i class="fas fa-eye"></i></a>
                                </button>
                                <button id="hide-${timestamp}"class="d-flex flex-column align-items-center my-auto gotodetail hide-it w-100 h-100 justify-content-center" style="background-color: white; border: none; border-radius: 0 0 10px 0">
                                    <a style="color: #a3310e;font-size: 20px;"><i class="fas fa-eye-slash"></i></a>
                                </button>
                            </div>
                        </div>
                    </div>
                `
                i++
            })

            document.querySelectorAll(".show-detail").forEach((item) => {
                item.addEventListener("click", () => {
                    const id = item.getAttribute("id")
                    const children = document.getElementById(`${id}`)
                    if (!children.classList.contains("animation")) {
                        children.classList.add("animation")
                        children.classList.remove("animation-back")
                        document.querySelector(`#z-index-removal-${id.slice(5, id.length)}`).classList.remove("my-z-index")
                    } else if (children.classList.contains("animation")) {
                        children.classList.remove("animation")
                        children.classList.add("animation-back")
                        document.querySelector(`#z-index-removal-${id.slice(5, id.length)}`).classList.add("my-z-index")
                    }
                })
            })

            document.querySelectorAll(".gotodetail").forEach((item) => {
                item.addEventListener("click", () => {
                    const id = item.getAttribute("id")
                    const children = document.getElementById(`item-${id.slice(5, id.length)}`)
                    if (item.classList.contains("hide-it")) {
                        children.classList.add("animation-back")
                        document.querySelector(`#z-index-removal-${id.slice(5, id.length)}`).classList.add("my-z-index")
                    }
                })
            })
        } else {
            document.querySelector("#history").innerHTML += `
                <div class="d-flex flex-lg-row flex-column align-items-center" style="position: fixed; left: 50%; transform: translate(-50%, -50%); top: 50%;">
                    <img style="width: 300px; height: 300px" src="../../src/assets/images/empty-cart.png" class=""/>
                    <div class="mx-5">
                        <p style="font-family: 'Viga', sans-serif; color: #3A3128;" >Hey, Your don't have history order before :(. Order Now!!</p>
                        <a style="border-radius: 25px; border: none; background-color: #F78430 !important; color: white;" class="btn btn-success w-50" id="orderEmptytohome">Order</a>
                    </div>
                </div>
            `

            document.querySelector("#orderEmptytohome").addEventListener('click', (event) => {
                event.preventDefault()
                location.replace("/#")
                location.reload()
            })
        }

        document.querySelector("#historytomain").addEventListener('click', (event) => {
            event.preventDefault()
            location.replace("/#")
            location.reload()
        })
    }
}

export default History