import UrlParser from '../routings/url-parser.js'

const PromoCode = {
    async render() {
        return `
        <div id="greetingForLogined" class="my-2 d-flex flex-row justify-content-between d-none justify-content-lg-center align-items-center">
            <div>
                <p class="mx-4 mb-0" style="font-size: 16px; color: #B6B6B6; font-family: 'Passion One', cursive;">Welcome Back,</p>
                <h3 id="lineName" class="mx-4 mb-0" style="font-size: 35px; color: black; font-family: 'Passion One', cursive; margin-top: -5px;"></h3>
            </div>
            <img id="profile-img" class="card-img mx-4" style="border-radius: 50%; width: 60px; height: 60px;" src='https://www.jobstreet.co.id/en/cms/employer/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png'>
        </div>
        <div class="row my-3 mx-lg-auto container">
          <div class="d-flex flex-row align-items-center">
            <a href="/#/cart" style="border: none; background-color: transparent; color: black">
                <i class="fas fa-chevron-left"></i>
            </a>
            <h2 class="mb-0 mx-3" style="color: black; font-family: 'Passion One', cursive;">Promo Code</h2>
            <hr>
          </div>
        </div>
        <div id="promo-list" class="mt-4 container-lg" style="margin-bottom: 125px;"></div>
        `
    },

    async afterRender() {
        const url = UrlParser.parseActiveUrlWithoutCombiner()
        const promos = ["MAKANAJA 20%", "HEMAT15 15rb"]
        let id = 0
        promos.forEach((promo) => {
            document.querySelector("#promo-list").innerHTML += `
                <div id="promo-${id}" class="position-relative mb-3 promo-code" style="cursor: pointer;">
                    <div class="d-flex flex-row align-items-center py-3 show-detail" style="background-color: white; border-radius: 10px;">
                        <div class="px-3 my-auto mx-3" style="padding-right: 10px !important">
                            <img src="../../src/assets/images/promo.png" width="75" height="75" style="border-radius: 8px; filter: drop-shadow(1px 4px 6px #E2E3E7);">
                        </div>
                        <div class="flex-row">
                            <div class="d-inline-block px-2" style="background-color: #FFF0EC; border-radius: 10px;">
                                <p class="text-center m-0" style="font-size: 12px; color: #D94B46; font-family: 'Viga'">Limited offer</p>
                            </div>
                            <p id="promoName" class="mb-0 mt-3" style="color: #303434; font-family: 'Viga', sans-serif;">${promo}</p>
                            <p class="mb-0" style="color: #A8ABB0; font-family: 'Viga', sans-serif; font-size: 10px;">Valid until 12 Jan 2022</p>
                        </div>
                    </div>
                </div>
            `
            id++
        })

        document.querySelectorAll(".promo-code").forEach((item) => {
            item.addEventListener('click', () => {
                const id = item.getAttribute("id")
                sessionStorage.setItem('promoOrder', JSON.stringify(promos[id.slice(6, id.length)]))

                location.replace("/#/cart")

            })
        })
    }
}

export default PromoCode