import UrlParser from '../routings/url-parser.js'

const Success = {
    async render() {
        return `
            <div id="greetingForLogined" class="d-none my-2 d-flex flex-row justify-content-between justify-content-lg-center align-items-center">
                <div>
                <p class="mx-4 mb-0" style="font-size: 16px; color: #B6B6B6; font-family: 'Passion One', cursive;">Welcome Back,</p>
                <h3 id="lineName" class="mx-4 mb-0" style="font-size: 35px; color: black; font-family: 'Passion One', cursive; margin-top: -5px;"></h3>
                </div>
                <img id="profile-img" class="card-img mx-4" style="border-radius: 50%; width: 60px; height: 60px;" src="" alt="">
            </div>
            <div id="paymentSuccess" class="mt-4 h-100 my-auto container-lg"></div>
        `
    },

    async afterRender() {
        const url = UrlParser.parseActiveUrlWithoutCombiner()

        document.querySelector("#paymentSuccess").innerHTML += `
            <div class="d-flex flex-xl-row flex-column align-items-center" style="position: fixed; left: 50%; transform: translate(-50%, -50%); top: 50%;">
                <img style="width: 300px; height: 300px" src="../../src/assets/images/success.png" class=""/>
                <div class="mx-5 mt-3">
                    <p style="font-family: 'Viga', sans-serif; color: #3A3128; text-align: center" >Yeayy!! Order Successfully Placed!</p>
                    <a id="successtomain" style="border-radius: 25px; border: none; background-color: #F78430 !important; color: white;" class="btn btn-success w-100" href="/#">Continue Order</a>
                    <a style="border-radius: 25px; border: none; color: #F78430; font-weight: bold" class="btn w-100 mt-2" href="/#/history/${url.id}">View Detail Order</a>
                </div>
            </div>
        `

        document.querySelector("#successtomain").addEventListener('click', (event) => {
            event.preventDefault()
            location.replace("/#")
            location.reload()
        })
    }
}

export default Success